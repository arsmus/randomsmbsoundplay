package net.sytes.rokkosan.randomsmbsoundplay

// 2021 Dec. 04.
// 2021 Oct. 30.
// 2021 Sep. 20.

// Ryuichi Hashimoto

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment

class SmbServerSettingDialogFragment: DialogFragment()  {

    // Thank for https://qiita.com/gino_gino/items/9390e1a0ce5e42d16466
    interface DialogListener{
        public fun onDialogMapReceive(dialog: DialogFragment, smbSettings: MutableMap<String, String>) //Activity側へMutableMapを渡す
    }

    var listener:DialogListener? = null

    // ダイアログ上のEditTextに文字列があるか無いかで buttonの enable を制御する
    class EditsWatcher(val button: Button, val edit1: EditText, val edit2: EditText, val edit3: EditText, val edit4: EditText):
        TextWatcher {
        init {
            edit1.addTextChangedListener(this)
            edit2.addTextChangedListener(this)
            edit3.addTextChangedListener(this)
            edit4.addTextChangedListener(this)
            afterTextChanged(null)
        }

        override fun beforeTextChanged(
            s: CharSequence?,
            start: Int,
            count: Int,
            after: Int
        ) {
        } //ignore

        override fun onTextChanged(
            s: CharSequence?,
            start: Int,
            before: Int,
            count: Int
        ) {
        } //ignore

        override fun afterTextChanged(s: Editable?) {
            button.isEnabled =
                edit1.text.toString().trim().isNotEmpty() &&
                        edit2.text.toString().trim().isNotEmpty() &&
                        edit3.text.toString().trim().isNotEmpty() &&
                        edit4.text.toString().trim().isNotEmpty()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val smbSettings: MutableMap<String, String> = mutableMapOf(
            "smbDomain" to "",
            "smbPath" to "",
            "smbUser" to "",
            "smbPassword" to "")

        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater
        val smbServerSettingView = inflater.inflate(R.layout.dialog_smb_server_setting, null)

        // Dialog上のEditTextオブジェクトを取得する
        val edDomain = smbServerSettingView.findViewById<EditText>(R.id.etDomain)
        val edSmbPath = smbServerSettingView.findViewById<EditText>(R.id.etSmbPath)
        val edSmbUser = smbServerSettingView.findViewById<EditText>(R.id.etSmbUser)
        val edSmbPassword = smbServerSettingView.findViewById<EditText>(R.id.etSmbPassword)

        // 呼び出し元でBundleにセットした値を取得し、Viewにセットする
        edDomain.setText(requireArguments().getString("smbDomain", ""))
        edSmbPath.setText(requireArguments().getString("smbRootPath", ""))
        edSmbUser.setText(requireArguments().getString("smbUser", ""))
        edSmbPassword.setText(requireArguments().getString("smbPassword", ""))

        builder.setView(smbServerSettingView)
            .setTitle("SMB Server Setting")
            .setPositiveButton("OK") { dialog, id ->
                with(smbSettings) {
                    put(
                        "smbDomain",
                        edDomain.text.toString()
                    )
                    put(
                        "smbPath",
                        edSmbPath.text.toString()
                    )
                    put(
                        "smbUser",
                        edSmbUser.text.toString()
                    )
                    put(
                        "smbPassword",
                        edSmbPassword.text.toString()
                    )
                }

                // pass data to the Activity having called this dialog
                listener?.onDialogMapReceive(this,smbSettings) ?: throw Exception("listener is null.")

            }
            .setNegativeButton("Cancel") { dialog, id ->
                // nothing is done
            }

        return builder.create().also { dialog ->  // dialogを返すと共にdialogにOnShowListenerをセットする
            dialog.setOnShowListener {
                val button = dialog.getButton(Dialog.BUTTON_POSITIVE)
                EditsWatcher(button, edDomain, edSmbPath, edSmbUser, edSmbPassword)
            }
        }
    }

    // DialogListenerインターフェースをlistenerにセットする
    //   MainActivityで実装したDialogListenerインターフェースのメソッドを利用できるようになる
    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as DialogListener
        }catch (e: Exception){
            Log.e("MyApp","CANNOT FIND LISTENER")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}
