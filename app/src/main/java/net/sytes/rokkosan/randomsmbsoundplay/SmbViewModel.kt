package net.sytes.rokkosan.randomsmbsoundplay

// 2021 Dec. 05.
// 2021 Oct. 30.
// 2021 Sep. 06.

// Ryuichi Hashimoto

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import jcifs.config.PropertyConfiguration
import jcifs.context.BaseContext
import jcifs.smb.NtlmPasswordAuthenticator
import jcifs.smb.SmbFile
import kotlinx.coroutines.*
import java.util.*

class SmbViewModel(private val user: String, private val password: String, private val domain: String, private val smbUrl: String): ViewModel() {
    private val audioExtension: String = "\\.mp3|\\.wav|\\.aac|\\.au|\\.gsm|\\.m4a|\\.ogg|\\.flac"

    lateinit var smb: SmbFile
    data class AudioFileProperty(val smbPath: String, val fileSize: Long)

    enum class ProcessStatus {
        NotYetGot,
        FromNow,
        Trying,
        Got,
        Failed
    }

    // LiveDataを設定する
    private var _smbConnectionStatus = MutableLiveData(ProcessStatus.NotYetGot)
    val smbConnectionStatus: LiveData<ProcessStatus>
        get() = _smbConnectionStatus

    private var _getAudioFileStatus = MutableLiveData(ProcessStatus.NotYetGot)
    val getAudioFileStatus: LiveData<ProcessStatus>
        get() = _getAudioFileStatus

    private val _smbPath: MutableLiveData<String> by lazy {
        // MutableLiveData<T>は、valでmutableな変数を宣言できる
        MutableLiveData<String>()
    }
    val smbPath: LiveData<String>
        // 下記により、smbPathの読み込み時は _smbPathが読み出される
        get() = _smbPath

    val _smbSize: MutableLiveData<Long> by lazy {
        MutableLiveData<Long>()
    }
    val smbSize: LiveData<Long>
        get() = _smbSize


    // Main process in this ViewModel
    fun connectSmbSelectAudioFile() {
        viewModelScope.launch(Dispatchers.IO) {
            // connect to SMB server in worker thread
            try {
                _smbConnectionStatus.postValue(ProcessStatus.Trying) // tyring to connect to SMB
                connectSmb()
            } catch (e: Exception) {
                _smbConnectionStatus.postValue(ProcessStatus.Failed) // Failed to connect to SMB"
                return@launch
            }
            // got connection to SMB
            _smbConnectionStatus.postValue(ProcessStatus.Got) // connected to SMB

            // select an audio file in SMB
            _getAudioFileStatus.postValue(ProcessStatus.Trying) // trying to get an audio file
            val audioFileProperty = selectAudioSmbFile()
            if ( audioFileProperty.fileSize < 1L ) {
                // no audio file
                _getAudioFileStatus.postValue(ProcessStatus.Failed) // failed to get an audio file
                smb.close()
            } else {
                // got an audio file
                _smbPath.postValue(audioFileProperty.smbPath)
                _smbSize.postValue(audioFileProperty.fileSize)
                _getAudioFileStatus.postValue(ProcessStatus.Got) // got an audio file

                // play audio in MainActivity

            }
        } // end of launch

        if ( ProcessStatus.Failed == smbConnectionStatus.value) {
            // Failed to connect to SMB
            return
        }
        if (ProcessStatus.Failed == getAudioFileStatus.value) {
            // Failed to get an audio file
            return
        }
    }

    // SMBサーバーに接続し、接続オブジェクトをsmbに入れる
    private fun connectSmb() {
        try {
            val prop = Properties()  // java.util.Properties
            prop.setProperty("jcifs.smb.client.minVersion", "SMB202")
            prop.setProperty("jcifs.smb.client.maxVersion", "SMB300")
            val baseCxt = BaseContext(PropertyConfiguration(prop))
            val auth =
                baseCxt.withCredentials(NtlmPasswordAuthenticator(domain, user, password))

            // connect to SMB server
            smb = SmbFile(smbUrl, auth)
            smb.list() // check if SMB-connection succeeded or failed
        } catch (e1: Exception) {
            try {
                smb.close()
            }
            catch (e2: Exception) {
                throw e2
            }
            throw e1
        }
    }

    /*
     * ランダムにSMBサーバーの音楽ファイルを1つ選び、そのパスとファイルサイズをAudioFilePropertyデータクラスで返す
     * 　data class AudioFileProperty(val smbPath: String, val fileSize: Long)
     * 失敗した時は AudioFileProperty("",0L) を返す。
     * 変数smbは、関数の外でjcifs-ngによって取得したSmbFileインスタンス
     * audioExtensionは、関数の外で設定された、音声ファイル拡張子集合の文字列
     */
    fun selectAudioSmbFile(): AudioFileProperty {
        try {
            if (smb.isDirectory) {
                // get all files in dir
                val smbNormalFiles: MutableList<SmbFile>
                val tmpSmbNormalFiles: MutableList<SmbFile> = mutableListOf()
                smbNormalFiles = getNormalSmbFiles(smb, tmpSmbNormalFiles)
                if (1 > smbNormalFiles.size) {
                    // No file in the directory
                    return AudioFileProperty("",0L)
                } else if (1 == smbNormalFiles.size) {
                    // only one file in the directory
                    if (!isMatchTail(smbNormalFiles[0].name, audioExtension)) {
                        // No audio file
                        return AudioFileProperty("",0L)
                    } else {
                        // Got one sound file.
                        // Play Audio in main thread
                        return AudioFileProperty(smbNormalFiles[0].path, smbNormalFiles[0].length())
                    }
                } else { // smbNormalFiles.size > 1
                    // in case of plural files.
                    // collect audio files to audioSmbFiles
                    val audioSmbFiles: MutableList<SmbFile> = mutableListOf()
                    for (eachSmbNormalFile in smbNormalFiles) {
                        if (isMatchTail(eachSmbNormalFile.name, audioExtension)) {
                            audioSmbFiles.add(eachSmbNormalFile)
                        }
                    }

                    if (1 > audioSmbFiles.size) {
                        // No audio file
                        return AudioFileProperty("",0L)
                    } else if (1 == audioSmbFiles.size) {
                        // Got one sound file.
                        // PlayAudio in main thread
                        return AudioFileProperty(audioSmbFiles[0].path, audioSmbFiles[0].length())
                    } else { // audioSmbFiles.size > 1
                        // select one audio file randomly
                        val countFiles: Int = audioSmbFiles.size
                        val random = Random()
                        val randomNum = random.nextInt(countFiles)
                        // Got one sound file.
                        // PlayAudio in main thread
                        return AudioFileProperty(audioSmbFiles[randomNum].path, audioSmbFiles[randomNum].length())
                    }
                }
            } else if (smb.isFile) {
                return if (!isMatchTail(smb.name, audioExtension)) {
                    // No audio file
                    AudioFileProperty("",0L)
                } else { // a normal file
                    // Got one sound file.
                    // PlayAudio in main thread
                    AudioFileProperty(smb.path, smb.length())
                }
            } else { // in case of smb is abnormal
                return AudioFileProperty("",0L)
            }
        } catch (e: Exception) {
            return AudioFileProperty("",0L)
        }
    }

    override fun onCleared() {
        try {
            smb.close()
        } catch (e: Exception) { }
        finally {
            viewModelScope.cancel()
            super.onCleared()
        }
    }

    /*
    * SMBサーバーのすべてのノーマルファイルを取得する再帰関数
    * パラメータ
    *   givenDir  取得対象のSMBディレクトリパス。
    *   tmpSmbNormalFiles  空のMutableList<SmbFile>型変数。
    *       1つ前の再帰関数実行結果を次の再帰関数に渡すための変数。
    *       ユーザーは最初にこの関数を起動するので、空のMutableListを与える。
    */
    private fun getNormalSmbFiles(givenDir: SmbFile, tmpSmbNormalFiles: MutableList<SmbFile>): MutableList<SmbFile> {
        val childSmbs = givenDir.listFiles()
        if (childSmbs.isNotEmpty()) {
            for (eachChild in childSmbs) {
                if (eachChild.isDirectory) {
                    getNormalSmbFiles(eachChild, tmpSmbNormalFiles)
                } else if (eachChild.isFile) {
                    tmpSmbNormalFiles.add(eachChild)
                } else {
                    continue
                }
            }
        }
        return tmpSmbNormalFiles
    }

    /*
     * target文字列の一番後ろのピリオド以降にconditionStr正規表現文字列が含まれていればtrueを返す
     */
    private fun isMatchTail(target: String, conditionStr: String): Boolean {
        // target中の一番後ろのピリオド（ファイル拡張子の区切り）の場所を検出する
        val idxStr = target.lastIndexOf(".")
        if (idxStr < 0) {
            return false
        }
        // targetから、ピリオドから末尾までの文字列を切り出す
        val extenStr = target.substring(idxStr)
        // 切り出した文字列がconditionStrに一致するか調べる
        val regex = (conditionStr+"$").toRegex(RegexOption.IGNORE_CASE)
        return regex.containsMatchIn(extenStr)
    }
}