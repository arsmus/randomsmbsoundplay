package net.sytes.rokkosan.randomsmbsoundplay
// 2022 Feb. 13.
// 2021 Dec. 28.
// 2021 Nov. 14.
// 2021 Oct. 31.
// 2021 Sep. 06.

// Ryuichi Hashimoto

import android.Manifest
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import net.sytes.rokkosan.randomsmbsoundplay.databinding.ActivityMainBinding
import java.io.*
import java.util.*
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity(),SmbServerSettingDialogFragment.DialogListener {

    //////////////////////////////////////////////
    // Please replace these values to your data //
    /*
    private var smbDomain: String = "192.168.1.1"
    private var smbRoot: String = "/SMB/SERVER/DIR/"
    private var smbUser: String = ""
    private var smbPassword: String = ""
    */
    private var smbDomain: String = "192.168.1.1"
    private var smbRoot: String = "/"
    private var smbUser: String = ""
    private var smbPassword: String = ""
    //////////////////////////////////////////////

    private var smbFilepath: String = ""
    private lateinit var binding: ActivityMainBinding
    private lateinit var audioUri: Uri

    val defaultSharedPreferences by lazy {PreferenceManager.getDefaultSharedPreferences(getApplicationContext())}

    lateinit var smbViewModel: SmbViewModel
    lateinit var smbViewModelFactory: SmbViewModelFactory
    lateinit var smbUrl: String

    private val PERMISSION_READ_EX_STOR: Int = 100
    lateinit var appContext: Context

    // option menu
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        val inflater = menuInflater
        inflater.inflate(R.menu.optionsmenu, menu)
        return true
    }

    //Option menuのitemがクリックされた時の動作
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.itemSmbServer -> {

                // ダイアログに渡す値をBundleにセット
                val args = Bundle()
                args.putString("smbDomain", smbDomain )
                args.putString("smbRootPath", smbRoot )
                args.putString("smbUser", smbUser )
                args.putString("smbPassword", smbPassword )

                val smbSettingdialog = SmbServerSettingDialogFragment()
                smbSettingdialog.setArguments(args)

                // ダイアログを表示
                smbSettingdialog.show(supportFragmentManager, "simple")
            }

            R.id.itemPlayedFileHistory -> {
                val playedFilesDialog = ListPlayedFilesDialogFragment()
                playedFilesDialog.show(supportFragmentManager, "simple")
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // SmbServerSettingDialogFragment.DialogListenerインターフェースのメソッドの実装
    override fun onDialogMapReceive(dialog: DialogFragment, smbSettings: MutableMap<String, String>) {
        // smbSettingdialogからの値を受け取る
        with ( defaultSharedPreferences.edit()) {
            putString("smbDomain", smbSettings["smbDomain"])
            putString("smbPath", smbSettings["smbPath"])
            putString("smbUser", smbSettings["smbUser"])
            putString("smbPassword", smbSettings["smbPassword"])
            apply()
        }

        smbDomain = smbSettings["smbDomain"] ?: ""
        smbRoot = smbSettings["smbPath"] ?: ""
        smbUser = smbSettings["smbUser"] ?: ""
        smbPassword = smbSettings["smbPassword"] ?: ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        appContext = applicationContext

        // get smb setting values
        with (defaultSharedPreferences) {
            smbDomain = getString("smbDomain", "") ?: ""
            smbRoot = getString("smbPath", "") ?: ""
            smbUser = getString("smbUser", "") ?: ""
            smbPassword = getString("smbPassword", "") ?: ""
        }

        // if any of values is empty, show dialog to get value
        if ( smbDomain.length < 1 ||
            smbRoot.length < 1 ||
            smbUser.length < 1 ||
            smbPassword.length < 1 ) {

            // smb設定ダイアログに渡す値をBundleにセット
            val args = Bundle()
            with(args) {
                putString("smbDomain", smbDomain )
                putString("smbRootPath", smbRoot )
                putString("smbUser", smbUser )
                putString("smbPassword", smbPassword )
            }

            val smbSettingdialog = SmbServerSettingDialogFragment()
            smbSettingdialog.setArguments(args)

            // smb設定ダイアログを表示
            smbSettingdialog.show(supportFragmentManager, "simple")
        }

        // リプレイボタン・リセットボタン
        if (binding.btnPlay.isEnabled) {
            binding.btnReplay.isEnabled = false
            binding.btnReset.isEnabled = true
            binding.btnQuit.isEnabled = true
        } else {
            binding.btnReplay.isEnabled = true
            binding.btnReset.isEnabled = true
            binding.btnQuit.isEnabled = true
        }

        // play-button to connect to SMB server and play audio
        binding.btnPlay.setOnClickListener {

            // Check network status.
            // Grateful for https://qiita.com/taowata/items/4609dcddc3ddb4840fd6

            // Network確認のため、ConnectivityManagerを取得する
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            // NetworkCapabilitiesの取得
            // 引数にcm.activeNetworkを指定し、現在アクティブなデフォルトネットワークに対応するNetworkオブジェクトを渡している
            val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)

            if (capabilities == null) {
                binding.tvInfo01.setText(R.string.noNetwork)
                playAndroidExternalAudio()
            } else {
                binding.tvInfo01.setText(R.string.waitMinutes)
                binding.btnPlay.isEnabled = false
                binding.btnReplay.isEnabled = true
                binding.btnReset.isEnabled = true
                binding.btnQuit.isEnabled = true

                // set smbUrl to prepare for connecting to SMB server
                smbUrl = "smb://" + smbDomain + smbRoot
                if (smbUrl.takeLast(1) != "/") {
                    smbUrl += "/"
                    // smbUrl文字列の末尾はスラッシュ（/）とする
                }

                /*
                 * get ViewModel for SmbConnection
                 */
                smbViewModelFactory = SmbViewModelFactory(smbUser, smbPassword, smbDomain, smbUrl)
                smbViewModel =
                    ViewModelProvider(this, smbViewModelFactory).get(SmbViewModel::class.java)

                /*
                 * create Observers
                 */
                // ファイルパス取得
                val pathObserver = Observer<String> { sPath ->
                    smbFilepath = sPath
                    binding.tvFilePath.text = sPath
                    binding.btnReplay.isEnabled = true
                    binding.btnReset.isEnabled = true
                }

                // ファイルサイズ取得
                val sizeObserver = Observer<Long> { sSize ->
                    binding.tvFileSize.setText(getString(R.string.kb, (sSize / 1000L).toString()))
                }

                // SMB接続状況に応じた処理
                val connectStatusObserver = Observer<SmbViewModel.ProcessStatus> { status ->
                    when (status!!) {
                        SmbViewModel.ProcessStatus.NotYetGot -> binding.tvInfo01.setText(R.string.notConnectedSmb)
                        SmbViewModel.ProcessStatus.FromNow -> binding.tvInfo01.setText(R.string.fromNowTryConnectSmb)
                        SmbViewModel.ProcessStatus.Trying -> binding.tvInfo01.setText(R.string.tryConnectingSmb)
                        SmbViewModel.ProcessStatus.Got -> binding.tvInfo01.setText(R.string.connectedSmb)
                        SmbViewModel.ProcessStatus.Failed -> {
                            binding.tvInfo01.setText(R.string.failedConnectSmb)
                            binding.tvInfo02.setText(R.string.tryGetAudioFile)

                            // play an audio file in external storage in android device
                            playAndroidExternalAudio()
                            binding.btnReplay.isEnabled = true
                            binding.btnReset.isEnabled = true
                        }
                    }
                }

                // SMBサーバーの音声ファイル取得状況に応じた処理
                val getSmbAudioFileStatusObserver = Observer<SmbViewModel.ProcessStatus> { status ->
                    when (status!!) {
                        SmbViewModel.ProcessStatus.NotYetGot -> binding.tvInfo02.setText(R.string.notGetSmbAudioFile)
                        SmbViewModel.ProcessStatus.FromNow -> binding.tvInfo02.setText(R.string.fromNowTryGetSmbAudioFile)
                        SmbViewModel.ProcessStatus.Trying -> binding.tvInfo02.setText(R.string.tryGetSmbAudioFile)
                        SmbViewModel.ProcessStatus.Got -> {
                            binding.tvInfo02.setText(R.string.gotSmbAudioFile)

                            // create and send Intent of audio-player
                            //   play with 3rd party media play application
                            try {
                                val audioIntent = Intent()
                                audioIntent.action = Intent.ACTION_VIEW
                                audioIntent.setDataAndType(Uri.parse(smbFilepath), "audio/*")
                                // if you decide VLC as the player, uncomment the next line
                                // audioIntent.setPackage("org.videolan.vlc")
                                startActivity(audioIntent)
                                binding.tvInfo01.setText(R.string.sentIntent)

                                // save audio-file-path to storage
                                lateinit var fw: FileWriter
                                val savePathFile =
                                    File(appContext.filesDir, getString(R.string.pathHistoryFile))
                                if (savePathFile.exists()) {
                                    fw = FileWriter(savePathFile, true)
                                } else {
                                    fw = FileWriter(savePathFile)
                                }
                                try {
                                    fw.write("$smbFilepath,");
                                } catch(e: IOException) {
                                    binding.tvInfo01.setText(e.toString())
                                } finally {
                                    try {
                                        fw.close()
                                    } catch(e: IOException) {
                                        binding.tvInfo01.setText(e.toString())
                                    }
                                }
                            } catch (e: Exception) {
                                binding.tvInfo01.setText(e.toString())
                            }
                        }

                        SmbViewModel.ProcessStatus.Failed -> { // SMBサーバーの音声ファイル取得に失敗
                            binding.tvInfo02.setText(R.string.failedGetSmbAudioFile)

                            // play an audio file in external storage in android device
                            binding.tvInfo01.setText(R.string.failedConnectSmb)
                            binding.tvInfo02.setText(R.string.tryPlayAndroidFile)
                            playAndroidExternalAudio()
                        }
                    }
                }

                // ViewModelでSMBサーバーに接続し音声ファイルを取得する
                smbViewModel.connectSmbSelectAudioFile()

                // Proceed according to status of SMB-connection in the screen
                smbViewModel.smbConnectionStatus.observe(this, connectStatusObserver)

                // Display filepath, filesize in the screen
                smbViewModel.smbPath.observe(this, pathObserver)
                smbViewModel.smbSize.observe(this, sizeObserver)

                // proceed according to status of getting SMB-audio-file
                smbViewModel.getAudioFileStatus.observe(this, getSmbAudioFileStatusObserver)
            }
        }

        // replay-button
        binding.btnReplay.setOnClickListener {
            binding.btnPlay.isEnabled = false
            binding.btnReplay.isEnabled = true
            binding.btnReset.isEnabled = true
            binding.btnQuit.isEnabled = true

            if ( "smb" == binding.tvFilePath.text.toString().substring(0, 3)) {
                // in case SMB
                // create and send Intent of audio-player
                //   play with 3rd party media play application
                try {
                    val audioIntent = Intent()
                    audioIntent.action = Intent.ACTION_VIEW
                    audioIntent.setDataAndType(Uri.parse(smbFilepath), "audio/*")

                    // if you decide VLC as the player, uncomment the next line
                    // audioIntent.setPackage("org.videolan.vlc")
                    startActivity(audioIntent)
                    binding.tvInfo01.setText(R.string.sentIntent)
                } catch (e: Exception) {
                    binding.tvInfo01.setText(e.toString())
                }
            } else {
                // in case to play audio in the storage in android device
                // パーミッションを取得しているのでオーディオファイルをIntentに乗せて発出する
                try {
                    val audioIntent = Intent()
                    audioIntent.action = Intent.ACTION_VIEW
                    audioIntent.setDataAndType(audioUri, "audio/*")
                    // if you decide VLC as the player, uncomment the next line
                    // audioIntent.setPackage("org.videolan.vlc")
                    startActivity(audioIntent)
                    binding.tvInfo02.setText(R.string.sentIntent)
                } catch (e: Exception) {
                    binding.tvInfo02.setText(e.toString())
                }
            }
        }

        // reset-button
        binding.btnReset.setOnClickListener {
            binding.btnPlay.isEnabled = true
            binding.btnReplay.isEnabled = false
            binding.btnReset.isEnabled = true
            binding.btnQuit.isEnabled = true

            // restart application
            try { smbViewModel.smb.close() } catch (e: Exception) { }
            val launchIntent = baseContext.packageManager
                .getLaunchIntentForPackage(baseContext.packageName)
            launchIntent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            finish()
            startActivity(launchIntent)
        }

        // quit-button
        binding.btnQuit.setOnClickListener {
            try { smbViewModel.smb.close()
            } catch (e: Exception) { }
            finishAndRemoveTask()
        }
    }

    // Activityのdestroy時のインスタンス保存
    //   onPauseの直後に呼ばれる
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val info01Text: String = binding.tvInfo01.text.toString()
        outState.putString("INFO01", info01Text)

        val info02Text: String = binding.tvInfo02.text.toString()
        outState.putString("INFO02", info02Text)

        val pathText: String = binding.tvFilePath.text.toString()
        outState.putString("PATH", pathText )

        val sizeText: String = binding.tvFileSize.text.toString()
        outState.putString("SIZE", sizeText )

        outState.putBoolean("btnPlay", binding.btnPlay.isEnabled)
        outState.putBoolean("btnReplay", binding.btnReplay.isEnabled)
        outState.putBoolean("btnReset", binding.btnReset.isEnabled)
        outState.putBoolean("btnQuit", binding.btnQuit.isEnabled)
    }

    // 保存されたインスタンスの読み込み
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        val info01Text: String = savedInstanceState.getString("INFO01") ?: ""
        binding.tvInfo01.setText(info01Text)

        val info02Text: String = savedInstanceState.getString("INFO02") ?: ""
        binding.tvInfo02.setText(info02Text)

        val pathText: String = savedInstanceState.getString("PATH") ?: ""
        binding.tvFilePath.setText(pathText)
        smbFilepath = pathText

        val sizeText: String = savedInstanceState.getString("SIZE") ?: ""
        binding.tvFileSize.setText(sizeText)

        binding.btnPlay.isEnabled = savedInstanceState.getBoolean("btnPlay")
        binding.btnReplay.isEnabled = savedInstanceState.getBoolean("btnReplay")
        binding.btnReset.isEnabled = savedInstanceState.getBoolean("btnReset")
        binding.btnQuit.isEnabled = savedInstanceState.getBoolean("btnQuit")
    }

    /*
     * create and send Intent of audio-player
     *  play with 3rd party media play application
     */
    private fun playAndroidExternalAudio() {
        // パーミッションがあれば処理を進める。無ければ取得する
        if ( isGrantedReadStorage()) {
            getPlayStorageMedia()
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                PERMISSION_READ_EX_STOR
            )
            // requestPermissions()の結果に応じた処理がonRequestPermissionsResult()で行われる
        }
    }

    // アプリにパーミッションが付与されているかどうかを確認するメソッド
    fun isGrantedReadStorage(): Boolean {
        return (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
    }

    // requestPermissions()の結果に対する処理を行うメソッド
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            PERMISSION_READ_EX_STOR -> {
                if (grantResults.isEmpty()) {
                    // STOP WORK
                    throw RuntimeException("Empty permission result")
                }
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission is got.
                    // YOU CAN CONTINUE YOUR WORK WITH EXTERNAL_STORAGE HERE
                    getPlayStorageMedia()

                } else {  // no permission is got
                    if (shouldShowRequestPermissionRationale(
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // permission request was denied.
                        // User declined, but system can still ask for more
                        // request permission agein
                        requestPermissions( arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), PERMISSION_READ_EX_STOR)
                    } else {
                        // permission request was denied and requested "no not ask".
                        // User declined and system can't ask.
                        // CODES in case of rejection to get permission here
                        Toast.makeText(applicationContext, R.string.noPermission, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    fun getPlayStorageMedia() {

        var pathOfUri: String= ""

        val collection =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                MediaStore.Audio.Media.getContentUri(
                    MediaStore.VOLUME_EXTERNAL
                )
            } else {
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            }

        val columns = arrayOf(
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.DISPLAY_NAME,
            MediaStore.Audio.Media.SIZE,
            "_data"
        )

        // 1分間以上のAudioを指定する
        val selection = "${MediaStore.Audio.Media.DURATION} >= ?"
        val selectionArgs = arrayOf(
            TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES).toString()
        )

        val resolver = applicationContext.contentResolver
        val query = resolver.query(
            collection,  //データの種類
            columns, //取得する項目 nullは全部
            selection, //フィルター条件 nullはフィルタリング無し
            selectionArgs, //フィルター用のパラメータ
            null   //並べ替え
        )
        // Log.d( "MyApp" , Arrays.toString( query?.getColumnNames() ) )  //項目名の一覧を出力
        val numCount = query?.count
        // Log.d("MyApp", "Num raws : $numCount")

        query?.use { cursor ->
            val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media._ID)
            val displayNameColumn =
                cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DISPLAY_NAME)
            val sizeColumn = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.SIZE)
            val column_index = cursor.getColumnIndexOrThrow("_data")

            cursor.move(getRandomNum(numCount!!))
            val id = cursor.getLong(idColumn)
            val displayName = cursor.getString(displayNameColumn)
            val mediaSize = cursor.getInt(sizeColumn)
            val contentUri: Uri = ContentUris.withAppendedId(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,id)
            pathOfUri = cursor.getString(column_index)
            binding.tvInfo02.setText(R.string.gotLocalAudioFile)
            binding.tvFilePath.setText(pathOfUri)
            val sizeKb = (mediaSize / 1000 ).toString() + "KB"
            binding.tvFileSize.setText(sizeKb)

            // play audio
            val audioIntent = Intent()
            audioIntent.action = Intent.ACTION_VIEW
            audioIntent.setDataAndType(Uri.parse(pathOfUri), "audio/*")
            // audioIntent.setDataAndType(contentUri, "audio/*")  <- NG
            audioIntent.setPackage("org.videolan.vlc")
            startActivity(audioIntent)
            binding.tvInfo02.setText(R.string.sentIntent)

            // save audio-file-path to storage
            lateinit var fw: FileWriter
            val savePathFile =
                File(appContext.filesDir, getString(R.string.pathHistoryFile))

            fw = if (savePathFile.exists()) {
                FileWriter(savePathFile, true)
            } else {
                FileWriter(savePathFile)
            }

            try {
                fw.write("$pathOfUri,");
            } catch(e: IOException) {
                binding.tvInfo01.setText(e.toString())
            } finally {
                try {
                    fw.close()
                } catch(e: IOException) {
                    binding.tvInfo01.setText(e.toString())
                }
            }

            /*
            Log.d(
                "MyApp", "id: $id, name: $displayName, ${mediaSize}Byte, uri: $contentUri"
            )
            */

            // Log.d("MyApp", "Selected " + pathOfUri!!)

        }
        query?.close()
        return
    }

    // 0以上、maxNum未満の範囲でランダムな数を1つ返す
    fun getRandomNum(maxNum: Int): Int {
        val random = Random()
        return random.nextInt(maxNum)
    }
}